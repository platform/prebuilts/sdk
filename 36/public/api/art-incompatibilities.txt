// Baseline format: 1.0
ChangedAbstract: java.util.AbstractSequentialList#listIterator(int):
    Method java.util.AbstractSequentialList.listIterator has changed 'abstract' qualifier

RemovedDeprecatedMethod: java.lang.Thread#countStackFrames():
    Removed deprecated method java.lang.Thread.countStackFrames()
RemovedDeprecatedMethod: java.lang.Thread#destroy():
    Removed deprecated method java.lang.Thread.destroy()
RemovedDeprecatedMethod: java.lang.Thread#resume():
    Removed deprecated method java.lang.Thread.resume()
RemovedDeprecatedMethod: java.lang.Thread#stop(Throwable):
    Removed deprecated method java.lang.Thread.stop(Throwable)
RemovedDeprecatedMethod: java.lang.Thread#suspend():
    Removed deprecated method java.lang.Thread.suspend()
